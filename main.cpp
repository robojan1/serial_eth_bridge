#include <iostream>

#include <linux/if.h>
#include <linux/if_tun.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <stdexcept>
#include <exception>
#include <sstream>
#include <sys/ioctl.h>

class TunnelInterface {
public:
	TunnelInterface() {
		Allocate();
	}

	~TunnelInterface() {
		close(_fd);
	}

	const std::string &name() const { return _name; }

	void test() {
		uint8_t buffer[1522+sizeof(tun_pi)];
		while(true) {
			ssize_t bytes_read = read(_fd, buffer, sizeof(buffer));
			if(bytes_read > 0) {
				struct tun_pi *info = (struct tun_pi *)(buffer);
				printf("Packet received: %zd %04x %04x\n", bytes_read - sizeof(tun_pi), info->flags, info->proto);
			}
		}
	}

private:
	int _fd;
	std::string _name;

	void Allocate() {
		struct ifreq ifr;

		_fd = open("/dev/net/tun", O_RDWR);
		if(_fd < 0) {
			std::stringstream ss;
			ss << "Could not open tunnel device: " << strerror(errno) << std::endl;
			throw std::runtime_error(ss.str());
		}

		memset(&ifr, 0, sizeof(ifr));

		ifr.ifr_flags = IFF_TUN;
		strcpy(ifr.ifr_name, "tun%d");
		int err = ioctl(_fd, TUNSETIFF, (void *)&ifr);
		if(err < 0) {
			close(_fd);
			std::stringstream ss;
			ss << "Could not request interface: " << strerror(errno) << std::endl;
			throw std::runtime_error(ss.str());
		}

		_name = ifr.ifr_name;
	}
};

int main() {
	std::cout << "Hello, World!" << std::endl;

	TunnelInterface ti;
	std::cout << "Tunnel " << ti.name() << " opened" << std::endl;

	ti.test();

	return 0;
}