// tun_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <Iphlpapi.h>

#include "tap-windows.h"
#include <vector>
#include <sstream>
#include <iomanip>
#include <array>
#include <minwinbase.h>

struct Adapter
{
	std::string componentId;
	std::string guid;
	std::string adapterName;
	std::string path;
};

struct MAC_Address
{
	uint8_t addr[6];

	std::string str() const
	{
		std::stringstream ss;
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[0]) << ':';
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[1]) << ':';
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[2]) << ':';
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[3]) << ':';
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[4]) << ':';
		ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(addr[5]);
		return ss.str();
	}
};

struct TAP_Version
{
	ULONG major;
	ULONG minor;
	ULONG debug;

	std::string str() const
	{
		std::stringstream ss;
		ss << major << '.' << minor;
		if (debug) ss << "-dbg";
		return ss.str();
	}
};

std::wstring stringToWstring(const std::string &str)
{
	return std::wstring(str.begin(), str.end());
}

DWORD GetTapAdapterIndex(const std::string& guid)
{
	size_t pathSize = sizeof("\\DEVICE\\TCPIP_") + guid.length();
	wchar_t* path = new wchar_t[pathSize];
	std::wstring pathCpp = L"\\DEVICE\\TCPIP_" + stringToWstring(guid);
	wcscpy_s(path, pathSize, pathCpp.c_str());
	DWORD idx;
	DWORD success = ::GetAdapterIndex(path, &idx);
	delete[] path;
	if(success != NO_ERROR)
	{
		throw std::runtime_error("Could not get device index");
	}
	return idx;
}

std::vector<Adapter> list_tap_adapter_guids()
{
	std::vector<Adapter> result;
	HKEY network_adapters_key;
	if(::RegOpenKeyExA(HKEY_LOCAL_MACHINE, ADAPTER_KEY, 0, KEY_READ, &network_adapters_key) != ERROR_SUCCESS)
	{
		throw std::runtime_error("Could not open adapter list key");
	}

	for(int idx = 0; ; idx++)
	{
		CHAR nameBuffer[256];
		DWORD nameBufferLen = sizeof(nameBuffer);

		// Get the next sub key
		LSTATUS status = ::RegEnumKeyExA(network_adapters_key, idx, nameBuffer, &nameBufferLen, nullptr, nullptr, nullptr, nullptr);
		if (status == ERROR_NO_MORE_ITEMS) {
			break;
		} else if (status != ERROR_SUCCESS) {
			throw std::runtime_error("Could not enumerate the network adapters");
		}
		nameBuffer[nameBufferLen] = '\0';

		// Create the sub key path
		std::string adapter_key = ADAPTER_KEY;
		adapter_key += "\\";
		adapter_key += nameBuffer;

		// Open the subkey
		HKEY network_adapter_key;
		status = ::RegOpenKeyExA(HKEY_LOCAL_MACHINE, adapter_key.c_str(), 0, KEY_READ, &network_adapter_key);
		if (status != ERROR_SUCCESS)
			continue;

		DWORD dataType;
		BYTE productNameBuffer[32];
		DWORD productNameBufferLen = sizeof(productNameBuffer);
		status = ::RegQueryValueExA(network_adapter_key, "ProductName", nullptr, &dataType, productNameBuffer, &productNameBufferLen);
		if(status != ERROR_SUCCESS || dataType != REG_SZ)
		{
			RegCloseKey(network_adapter_key);
			continue;
		}
		productNameBuffer[productNameBufferLen] = '\0';
		if(strcmp("TAP-Windows Adapter V9", (const char *)productNameBuffer) != 0)
		{
			RegCloseKey(network_adapter_key);
			continue;
		}

		BYTE guid[64];
		DWORD guidLen = sizeof(guid);
		status = ::RegQueryValueExA(network_adapter_key, "NetCfgInstanceId", nullptr, &dataType, guid, &guidLen);
		if (status != ERROR_SUCCESS || dataType != REG_SZ)
		{
			RegCloseKey(network_adapter_key);
			continue;
		}

		guid[guidLen] = '\0';

		BYTE componentId[64];
		DWORD componentIdLen = sizeof(guid);
		status = ::RegQueryValueExA(network_adapter_key, "ComponentId", nullptr, &dataType, componentId, &componentIdLen);
		if (status != ERROR_SUCCESS || dataType != REG_SZ)
		{
			RegCloseKey(network_adapter_key);
			continue;
		}

		componentId[componentIdLen] = '\0';

		RegCloseKey(network_adapter_key);

		// Create connectionKey key
		std::string connectionKeyName = NETWORK_CONNECTIONS_KEY "\\" + std::string((char *)guid) + "\\Connection";
		HKEY connectionKey;
		status = ::RegOpenKeyExA(HKEY_LOCAL_MACHINE, connectionKeyName.c_str(), 0, KEY_READ, &connectionKey);
		if (status != ERROR_SUCCESS)
			continue;

		BYTE adapterName[64];
		DWORD adapterNameLen = sizeof(adapterName);
		status = ::RegQueryValueExA(connectionKey, "Name", nullptr, &dataType, adapterName, &adapterNameLen);
		if (status != ERROR_SUCCESS || dataType != REG_SZ)
		{
			RegCloseKey(connectionKey);
			continue;
		}

		adapterName[adapterNameLen] = '\0';
		RegCloseKey(connectionKey);

		// Create the result
		struct Adapter elem;
		elem.componentId = (char *)componentId;
		elem.guid = (char *)guid;
		elem.adapterName = (char*)adapterName;
		elem.path = std::string(USERMODEDEVICEDIR) + elem.guid + TAP_WIN_SUFFIX;

		result.emplace_back(elem);
	}

	RegCloseKey(network_adapters_key);
	return result;
}

class TapInterface
{
public:
	TapInterface(const std::string &path) : _handle(INVALID_HANDLE_VALUE)
	{
		Open(path);
	}
	~TapInterface()
	{
		Close();
	}

	void start()
	{
		ZeroMemory(&_rxOverlapped, sizeof(_rxOverlapped));
		_rxOverlapped.hEvent = this;
		BOOL status = ReadFileEx(_handle, _rxBuffer.data(), _rxBuffer.size(), &_rxOverlapped, &TapInterface::staticReadCompleted);
		if (!status || GetLastError() != ERROR_SUCCESS)
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not start reading from the tun interface");
		}
	}

	void stop()
	{
		CancelIo(_handle);
	}

	void send(const void *data, int size)
	{
		int total_written = 0;
		while(total_written < size)
		{
			DWORD written = 0;
			if(!WriteFile(_handle, reinterpret_cast<const uint8_t*>(data) + total_written, size - total_written, &written, nullptr))
			{
				DWORD error = GetLastError();
				throw std::runtime_error("Could not send data to the tap device");
			}
			total_written += written;
		}
	}

	struct MAC_Address macAddress() const
	{
		struct MAC_Address buffer;
		DWORD bufferLen;
		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_GET_MAC, &buffer, sizeof(buffer), &buffer, sizeof(buffer), &bufferLen, nullptr);
		if (result == 0 || bufferLen != sizeof(buffer))
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not get the mac address");
		}
		return buffer;
	}

	struct TAP_Version version() const
	{
		struct TAP_Version buffer;
		DWORD bufferLen;
		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_GET_VERSION, &buffer, sizeof(buffer), &buffer, sizeof(buffer), &bufferLen, nullptr);
		if (result == 0 || bufferLen != sizeof(buffer))
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not get the adapter version");
		}
		return buffer;

	}

	ULONG mtu() const
	{
		ULONG mtu;
		DWORD bufferLen;
		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_GET_MTU, &mtu, sizeof(mtu), &mtu, sizeof(mtu), &bufferLen, nullptr);
		if (result == 0 || bufferLen != sizeof(mtu))
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not get the adapter MTU");
		}
		return mtu;
	}

	// Not yet implemented in the driver
	std::string info() const
	{
		char buffer[256];
		DWORD bufferLen;
		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_GET_INFO, &buffer, sizeof(buffer), &buffer, sizeof(buffer), &bufferLen, nullptr);
		if (result == 0)
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not get the adapter MTU");
		}
		return buffer;
	}

	void setMediaStatus(bool connected)
	{
		ULONG status = connected ? 1 : 0;
		DWORD bufferLen;
		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_SET_MEDIA_STATUS, &status, sizeof(status), &status, sizeof(status), &bufferLen, nullptr);
		if (result == 0)
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not set the media status");
		}
	}

	void configTun(uint32_t ip, int netmaskLen)
	{
		uint32_t netmask = 0;
		for(int i = 0; i < netmaskLen; i++)
		{
			netmask = (netmask >> 1) | (1 << 31);
		}
		uint32_t buffer[3] = { htonl(ip), htonl(ip & netmask), htonl(netmask) };
		DWORD bufferLen;

		BOOL result = ::DeviceIoControl(_handle, TAP_WIN_IOCTL_CONFIG_TUN, &buffer, sizeof(buffer), &buffer, sizeof(buffer), &bufferLen, nullptr);
		if (result == 0)
		{
			DWORD error = GetLastError();
			throw std::runtime_error("Could not configure the device in TUN mode");
		}
	}

private:
	HANDLE _handle;
	std::array<uint8_t, 2048> _rxBuffer;
	OVERLAPPED _rxOverlapped;

	void Open(const std::string &path)
	{
		_handle = ::CreateFileA(path.c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_SYSTEM | FILE_FLAG_OVERLAPPED, 0);
		if(_handle == INVALID_HANDLE_VALUE)
		{
			throw std::runtime_error("Could not open network adapter");
		}
	}

	void Close()
	{
		stop();
		::CloseHandle(_handle);
	}

	void readCompleted(DWORD dwErrorCode, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED lpOverlapped)
	{
		printf("Packet received of size %d\n", dwNumberOfBytesTransferred);

		BOOL status = ReadFileEx(_handle, _rxBuffer.data(), _rxBuffer.size(), &_rxOverlapped, &TapInterface::staticReadCompleted);
		if (!status || GetLastError() != ERROR_SUCCESS)
		{
			throw std::runtime_error("Could not start reading from the tun interface");
		}
	}

	static void WINAPI staticReadCompleted(DWORD dwErrorCode, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED lpOverlapped)
	{
		auto self = reinterpret_cast<TapInterface*>(lpOverlapped->hEvent);
		self->readCompleted(dwErrorCode, dwNumberOfBytesTransferred, lpOverlapped);
	}


};

int main()
{
    std::cout << "Hello World!\n";
	auto adapters = list_tap_adapter_guids();
	printf("Adapters:\n");
	for(auto &adapter : adapters)
	{
		int idx = GetTapAdapterIndex(adapter.guid);
		printf("\t[%s] - %s - [%d]%s - %s\n", adapter.guid.c_str(), adapter.componentId.c_str(), idx, adapter.adapterName.c_str(), adapter.path.c_str());
	}

	if (adapters.size() == 0) return 1;

	auto& adapterInfo = adapters[0];
	printf("Opening adapter %s\n", adapterInfo.adapterName.c_str());
	TapInterface ti(adapterInfo.path);
	printf("MAC address: %s\n", ti.macAddress().str().c_str());
	printf("Version: %s\n", ti.version().str().c_str());
	printf("MTU: %u\n", ti.mtu());
	//printf("Info: %s\n", ti.info().c_str());

	printf("\n");
	printf("Enabling the connection\n");
	ti.setMediaStatus(true);
	printf("Setting the IP\n");
	ti.configTun((10 << 24) | (50 << 16) | (2 << 8) | (1 << 0),16);
	ti.start();

	//ti.send("Hello world", 11);
	for(DWORD dwStart = GetTickCount();;)
	{
		DWORD dwElapsed = GetTickCount() - dwStart;
		DWORD dwTimeout = 60 * 1000;
		if (dwElapsed > dwTimeout) break;
		SleepEx(60 * 1000 - dwElapsed, TRUE);
	}

	printf("Disabling the connection\n");
	ti.stop();
	ti.setMediaStatus(false);

	return 0;
}
